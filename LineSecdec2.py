#LineSecdec2

import numpy as np
import cv2
import imutils
import math
from shapely.geometry import LineString
import pytesseract

# load des fonctions : Haar,factor func, contour finder  fill contour et intersections finder, order_points

def Haar_faces_based_ID_finder(image,scale_percent = 50):
    pass
    # We point OpenCV's CascadeClassifier function to where our 
    # classifier (XML file format) is stored
    url="C:/Users/batma_000/Desktop/cours python data/Reco Passeport/cascades/haarcascades/haarcascade_frontalface_default.xml"
    face_classifier = cv2.CascadeClassifier(url)
    
    
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    copy=image.copy()
    
    
    
    width_image = int(image.shape[1] * scale_percent / 100)
    height_image = int(image.shape[0] * scale_percent / 100)
    dim_image = (width_image, height_image)
    
    width_roi = int(image.shape[1] * scale_percent / 100)
    height_roi = int(image.shape[0] * scale_percent / 100)
    dim_roi = (width_roi, height_roi)
    
    
    
    resized = cv2.resize(image, dim_image, interpolation = cv2.INTER_AREA)


    
    # Our classifier returns the ROI of the detected face as a tuple
    # It stores the top left coordinate and the bottom right coordiantes
    faces = face_classifier.detectMultiScale(gray, 1.3, 5)
    
    if faces is ():
        print("No faces found")
        roi = image

    else :
        #we sort the faces found on area of the rectangle returned by Haar detection. 
        #We assume the biggest face is always placed on the top-left on an ID document.
        sorted_faces = sorted(faces,  key=lambda faces: faces[2]*faces[3], reverse = True)
        (xi,yi,w,h)=sorted_faces[0]
        
        #rect = cv2.rectangle(image.copy(), (xi, yi), (xi+w , yi +h ), (0, 0, 255), 5)
        #resized = cv2.resize(rect, dim_image, interpolation = cv2.INTER_AREA)
        #cv2.imshow('face',resized)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()
        print(xi,yi,w,h)

        H = 2*h
        W = 2*w
        print("deltaX,deltaY",(xi-w,yi-h))

        if ((xi-W)<0) & ((yi-H)<0):
            x0 = xi-xi +1
            y0=yi-yi +1

        elif ((xi-W)<0) & ((yi-H)>0):
            x0 = xi-xi +1
            y0 = yi-int(H)

        elif ((xi-W)>0) & ((yi-H)<0):
            x0 = xi-int(W)  
            y0=yi-yi +1

        else : 
            x0 = xi-int(W)  
            y0 = yi-int(H)

        print("coordinates of rectangle origin",x0,y0)
        print(image.shape)

        rectangle_width =int(10*w)
        rectangle_height = int(5*h)

        if (rectangle_width>(image.shape[1]-x0)) & (rectangle_height>(image.shape[0]-y0)):

            rectangle_width =(image.shape[1]-x0) 
            rectangle_height = (image.shape[0]-y0)

            cv2.rectangle(copy, (x0, y0), (x0 + rectangle_width , y0 + rectangle_height ), (0, 255, 0), 10)

        elif (rectangle_width>(image.shape[1]-x0)) & (rectangle_height<(image.shape[0]-y0)):

            rectangle_width =(image.shape[1]-x0)
            rectangle_height = rectangle_height

            cv2.rectangle(copy, (x0, y0), (x0 +(image.shape[1]-x0) , y0 + rectangle_height ), (0, 255, 0), 10)

        elif (rectangle_width<(image.shape[1]-x0)) & (rectangle_height>(image.shape[0]-y0)):

            rectangle_width =rectangle_width
            rectangle_height = (image.shape[0]-y0)


            cv2.rectangle(copy, (x0, y0), (x0 + rectangle_width , y0 + (image.shape[0]-y0) ), (0, 255, 0), 10)

        else:
            rectangle_width =rectangle_width
            rectangle_height = rectangle_height


            cv2.rectangle(copy, (x0, y0), (x0 +rectangle_width , y0 + rectangle_height ), (0, 255, 0), 10)



        roi = image[y0:y0 + rectangle_height,x0:x0 + rectangle_width]

    return roi
def factor_func(x1,y1,x2,y2,image,vertical_fact=20):
    pass

    xm,ym = (x1+x2)/2,(y1+y2)/2
    xi = int(image.shape[1]/2)
    yi = int(image.shape[0]/2)

    a = image.shape[1]
    b = image.shape[0]
    
    #vecteur directeur de la droite
    V_12 = np.array([x2,y2])-np.array([x1,y1])
    
    #vecteur abcisse
    O_i = np.array([1,0])
    
    #normalisation 
    unit_vector_1 = V_12 / np.linalg.norm(V_12)
    unit_vector_2 = O_i / np.linalg.norm(O_i)
    
    #scalaire
    pdt_scal = np.dot(unit_vector_1,unit_vector_2)
    alpha = np.arccos(pdt_scal)
    
    
    z = (((xm-xi)/(a/2))**2 + ((ym-yi)/(b/2))**2)
    
    pi=np.pi
    if -(30*pi/180)+pi/2<alpha<(pi/2)+(30*pi/180):
        z = vertical_fact*z
    else:
        z=z


    
    return z
def largest_contour_finder(img):
    
    img2 = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)

    # Find the contours 
    cnts = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    cnt = None
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[0]
    # For each contour, find the convex hull and draw it
    # on the original image.
    largest_area=0
    for c in cnts:
        cv2.drawContours(img2, [c], -1, (0, 255, 0),1)



    cv2.imshow('contours', img2)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return cnts
def countour_filler(img,cnts):
    if len(img.shape)==3:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    elif len(img.shape)==2:
        gray = img
    
    
    mask4 = np.zeros((gray.shape),np.uint8)


    fillin = cv2.fillPoly(gray, pts =[cnts], color=(255,255,255))
    return gray
def intersections_finder(img):




    #Create default parametrization LSD
    lsd = cv2.createLineSegmentDetector(0)

    if len(img.shape)==3:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    elif len(img.shape)==2:
        gray = img
    else :
        print("Argument takes only image.shape = 2 or 3")



    lines = lsd.detect(gray)[0] #Position 0 of the returned tuple are the detected lines

    #Draw detected lines in the image
    mask1 = np.zeros((gray.shape),np.uint8)
    mask2 = np.zeros((gray.shape),np.uint8)

    lines_sort = sorted(lines, key = lambda l :(l[0][2]-l[0][0])**2+(l[0][3]-l[0][1])**2, reverse=True)[:80]


    lines_sort_flatten =[]
    lines_sort_enhanced = []
    for l in lines_sort[:80]:
        x1, y1, x2, y2 = l.flatten()


        line_length = np.sqrt((x2-x1)**2+(y2-y1)**2)

        fact = factor_func(x1,y1,x2,y2,mask2,10)
        ratio = (0.001*line_length)*(fact*2)
        ratio=ratio
        enlarge_factor = np.array([ratio,ratio])
        #print(enlarge_factor)


        #Vector

        V = np.array([x2,y2])-np.array([x1,y1])

        a = np.array([x2,y2])+V*enlarge_factor
        b= np.array([x1,y1])-enlarge_factor*V


        x0,y0=b[0],b[1]
        x3,y3 = a[0],a[1]
        x0 = int(x0)
        y0 = int(y0)
        x3 = int(x3)
        y3 = int(y3)
        x1 = int(x1)
        y1 = int(y1)
        x2 = int(x2)
        y2 = int(y2)
        #print(x0,y0,x3,y3)
        lines_sort_enhanced.append((x0,y0,x3,y3))
        lines_sort_flatten.append((x1,y1,x2,y2))
        cv2.line(mask1, (x1, y1), (x2,y2),255, 2, cv2.LINE_AA)
        cv2.line(mask2, (x0, y0), (x3,y3),255, 2, cv2.LINE_AA)

    cv2.imshow("mask1_sans_facteur",mask1)
    cv2.imshow("mask2_avec_facteur",mask2)
    cv2.waitKey(0)
    cv2.destroyAllWindows()





    intersections = []
    segment = []


    for i, lineI in enumerate(lines_sort_enhanced):
        line1 = LineString([(lineI[0],lineI[1]),(lineI[2],lineI[3])])

        #define Normalized Vector
        x1 = line1.coords[0][0]
        y1 = line1.coords[0][1]
        x2 = line1.coords[1][0]
        y2 = line1.coords[1][1]
        line_length = np.sqrt((x2-x1)**2+(y2-y1)**2) 
        v1_norm = (np.array([x2,y2])-np.array([x1,y1]))/line_length

        for lineJ in lines_sort_enhanced[i+1:]:

            line2 = LineString([(lineJ[0],lineJ[1]),(lineJ[2],lineJ[3])])
            #define Normalized Vector

            x3 = line2.coords[0][0]
            y3 = line2.coords[0][1]
            x4 = line2.coords[1][0]
            y4 = line2.coords[1][1]
            line_length = np.sqrt((x4-x3)**2+(y4-y3)**2) 
            v2_norm = (np.array([x4,y4])-np.array([x3,y3]))/line_length

            if str((line1.intersection(line2)).type)=="Point":
                x3 = line2.coords[0][0]
                y3 = line2.coords[0][1]
                x4 = line2.coords[1][0]
                y4 = line2.coords[1][1]
                angle = np.arccos(np.dot(v1_norm,v2_norm))
                angle_deg = angle*180/np.pi
                #angle compris entre 0 et 180 deg
                if (70<angle_deg<110):
                    Px = (line1.intersection(line2)).xy[0][0]
                    Py = (line1.intersection(line2)).xy[1][0]
                    Px= int(Px)
                    Py= int(Py)
                    if (0<=Px<=img.shape[1]) and (0<=Py<=img.shape[0]):
                        intersections.append((Px,Py))

    return intersections, mask2
def order_points(list_intersect):
    sorted_list_sum = sorted(list_intersect,key =lambda l: l[0]+l[1])
    sorted_list_diff = sorted(list_intersect,key =lambda l: l[0]-l[1])
    rect = np.zeros((4, 2), dtype = "float32")
    rect[0]=sorted_list_sum[0]
    rect[2]=sorted_list_sum[-1]
    rect[1] = sorted_list_diff[-1]
    rect[3]=sorted_list_diff[0]
    return rect
def load_img():
    url_image = 'C:/Users/batma_000/Desktop/cours python data/Reco Passeport/samples/Scan/AustrPass.jpg'
    img = cv2.imread(url_image)

    roi = Haar_faces_based_ID_finder(img,100)

    
    
    ratio = roi.shape[0] / 500
    orig = roi.copy()
    roi = imutils.resize(roi, height = 500)
    
    return roi,orig
def four_point_transform(image, list_intersect):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(list_intersect)
    (tl, tr, br, bl) = rect
    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    # return the warped image
    return warped

img = load_img()[0]
orig = load_img()[1]

list_of_intersect, mask2  = intersections_finder(img)
list_of_intersect, mask2  = intersections_finder(mask2)

for Px,Py in list_of_intersect:
    cv2.circle(img,(Px,Py),1,(255,255,255),2)    


cv2.imshow("img_intersect",img)
cv2.waitKey(0)
cv2.destroyAllWindows()

mask3 = np.zeros((mask2.shape),np.uint8)

largest_countour = largest_contour_finder(mask3)
fillin = countour_filler(mask3,largest_countour)

cv2.imshow('fillin', fillin)
cv2.waitKey(0)
cv2.destroyAllWindows()

mask5 = cv2.bitwise_and(mask2, fillin)

largest_countour = largest_contour_finder(mask5)
fillin2 = countour_filler(mask5,largest_countour)

kernel = np.ones((13,13),np.uint8)
opening = cv2.morphologyEx(fillin2, cv2.MORPH_OPEN, kernel)
largest_countour = largest_contour_finder(opening)

largest_countour_tupleized=[]
for i in range (len(largest_countour)):
    x = largest_countour[i][0][0]
    y = largest_countour[i][0][1]
    largest_countour_tupleized.append((x,y))

img = img
cv2.drawContours(img, [largest_countour], -1, (0, 255, 0), 2)
cv2.imshow('img_contours', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

order_points(largest_countour_tupleized_orig)

for Px,Py in rect:
    cv2.circle(img,(Px,Py),1,(0,255,0),2)    


cv2.imshow("img_intersect",img)
cv2.waitKey(0)
cv2.destroyAllWindows()

img2 = four_point_transform(orig,largest_countour_tupleized_orig)


resized = imutils.resize(img2, height = 500)

cv2.imshow('resized_img2', resized)

cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imshow('img2', img2)

cv2.waitKey(0)
cv2.destroyAllWindows()




    

